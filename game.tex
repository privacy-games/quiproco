\documentclass[a4paper, 12pt]{article}
\usepackage{fullpage}
\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{color}
\usepackage{tikz}
\usepackage{url}
\usepackage{import}
\usepackage{caption,wrapfig}

\setlength{\parskip}{\smallskipamount}
\setlength{\parindent}{0pt}

\usepackage{csquotes}

\newenvironment{encart}[2]
{
  \begin{center}
  \vbox{
  \colorbox{light-blue}{
  \begin{minipage}{\linewidth}
  \begin{center}\textbf{#1}\end{center}
  \end{minipage}}\\
  \colorbox{light-gray}{
  \begin{minipage}{\linewidth}
  #2
  \end{minipage}}
  }
  \end{center}
}

\AtBeginDocument{
  \def\labelitemi{$\bullet$}
  \def\labelitemii{$\bullet$}
  \def\labelitemiii{$\bullet$}
}

%\boldmath 

\definecolor{light-gray}{gray}{0.95}
\definecolor{light-blue}{rgb}{0.7 0.7 0.9}

\begin{document}

\begin{center}
  \Huge \textbf{QuiProCo} \\
  \vspace{5pt} \large \textbf{QUI} est-ce, ré\textbf{P}onse
  \textbf{R}and\textbf{O}misée, \textbf{CO}nfidentialité
  différentielle
\end{center}


\begin{center}
  \label{fig:cat}
  \includegraphics[scale=0.9]{illustration.png}
\end{center}


Suite au développement d'internet et des outils de stockage
d'information, il est aujourd'hui possible d'accumuler de grandes
quantités de données personnelles. Cette accumulation de données est à
la fois une chance et un danger. D'une part, la collecte de données
permet d’acquérir des informations sur la population et de mener des
analyses diverses. On peut ainsi traquer l'évolution d'une maladie,
mesurer le niveau de pauvreté, optimiser les moyens de transports,
\textit{etc}.  D'un autre coté, la collecte, le stockage, et les
divers usages de ces données personnelles augmentent les risques
d'atteinte à la vie privée des personnes concernées.

La \emph{differential privacy}~\cite{differential-privacy, TCS-042} a
pour objectif d'autoriser les usages secondaires des données
personnelles (par exemple, l'analyse statistique, l'apprentissage
automatique) tout en conservant des garanties fortes de protection des
informations concernant chaque individu.

Le but de cette activité, basée sur le jeu \emph{Qui est-ce ?}, est
d'illustrer intuitivement les différentes propriétés mathématiques de
la differential privacy ainsi qu'un algorithme simple satisfaisant la
differential privacy appelé \emph{le mécanisme de réponse
  randomisée}. Plusieurs variantes de cet algorithme existent. Cette
activité met en oeuvre la variante suivante. Un.e statisticien.ne
sonde une population sur un sujet sensible. Lorsqu'une question est
posée à un individu, celui-ci répond soit honnêtement soit
aléatoirement. La probabilité de répondre aléatoirement est
fixée. Ainsi les résultats statistiques de l'enquête seront plus ou
moins perturbés en fonction de cette probabilité, mais il ne sera pas
possible de déterminer la véracité d'une réponse d'un individu. En
particulier, tout individu ayant répondu sera capable d'attribuer sa
réponse à l'aléa du mécanisme de manière plausible.


\encart{Matériel}{
  \begin{itemize}
  \item Des cartes \enquote{personnage} offrant des caractéristiques
    variables et des distributions différentes.
  \item Des dés (ou toute autre source d'aléa).
  \item Des cartes \enquote{objectifs statistiques}.
  \item Des cartes \enquote{objectifs personnels}.
  \end{itemize}
}


\section*{Déroulement de l'activité}

Tout d'abord, deux participants à l'activité sont désignés dans le
groupe pour être \enquote{l'enquêteur} et \enquote{l'attaquant}. Ce
sont eux qui poseront les questions aux autres joueurs, qui seront
alors les \enquote{répondants}. \enquote{L'enquêteur} et
\enquote{l'attaquant} pourront poser leurs questions à tour de rôle
par exemple.

L'enquêteur a une ou plusieurs cartes \enquote{objectif
  statistique}, et l'attaquant une ou plusieurs cartes
\enquote{objectif attaque}. L'enquêteur cherche à obtenir des
statistiques précises sur la population permettant de répondre à ses
cartes \enquote{objectif statistique}, tandis que l'attaquant cherche
à inférer des caractéristiques précises d'un ou plusieurs
répondants.

Les répondants répondent à l'enquêteur et à l'attaquant de la même
manière, en suivant le protocole suivant.
\begin{itemize}
\item L'enquêteur (ou l'attaquant) pose une question binaire (dont la
  réponse est \enquote{oui} ou \enquote{non}) aux répondants.
\item Chaque répondant répond honnêtement avec probabilité $p$ (par
  exemple, $p=2/3$) et uniformément aléatoirement avec la probabilité
  complémentaire (par exemple, $1-p=1/3$). L'aléa est généré par deux
  lancers d'un dé (en séquence) ou bien par un lancer de deux dés (en
  parallèle). L'important est de toujours lancer deux dés et de ne pas
  dévoiler les résultats du dé à l'enquêteur (ni à l'attaquant).
\item L'enquêteur (ou l'attaquant) note les réponses\footnote{Nous
    recommandons d'attribuer un identifiant aux répondants afin de
    faciliter la prise de notes de l'attaquant (par exemple, un
    nombre). Chaque répondant indiquera son identifiant à chaque
    réponse (par exemple, en l'écrivant sur une feuille montrée à
    chaque réponse).} (sur un tableau visible de tous, ou bien sur un
  bloc-note caché aux répondants). Impossible de savoir avec certitude
  si une réponse donnée est honnête ou aléatoire (le répondant peut
  toujours nier l'honnêteté de sa réponse en l'attribuant au jet de
  dé)
\item Lorsque l'enquêteur a terminé son sondage, la partie prend fin. 
\item L'enquêteur calcule ses statistiques sur la population et
  l'attaquant réalise ses inférences sur chaque répondant ciblé. Les
  résultats sont dévoilés aux répondants. Il est attendu que les
  statistiques de l'enquêteur sur les tendances générales de la
  population soient proches des statistiques réelles, tandis que les
  inférences de l'attaquant soient imprécises ou bien soulignent
  les limites de la differential privacy.
\end{itemize}

Des variantes peuvent être imaginées, comme par exemple la contrainte
pour l'attaquant de ne pas être démasqué par les répondants, ou
l'utilisation par l'attaquant de connaissances auxiliaires sur les
répondants.


\section*{Objectif pédagogique et complément scientifique}
Le but de cette activité est d'illustrer une technique de protection
de la vie privée.  Normalement l'objectif statistique devrait être rempli
tandis que l'objectif attaque devrait échouer. Il s'agit de
sensibiliser au fait qu'il est possible de collecter des données
utiles tout en limitant l'impact sur la vie privée.


La differential privacy est une famille de modèles formels de vie
privée proposée par Cynthia Dwork en 2006~\cite{differential-privacy,
  TCS-042}. Cette famille de modèles est la référence actuelle en
matière de protection de la vie privée dans la publication de données
au sens large. Intuitivement, la differential privacy cherche à
garantir que la présence ou l'absence de tout individu (ou bien la
valeur de ses données) n'ait que peu d'impact sur le résultat de
l'analyse statistique. En particulier, le mécanisme de réponse
randomisée permet de satisfaire la differential privacy. Le mécanisme
de réponse randomisée a été introduit par le sociologue Stanley Warner
dans le milieu des années 1960~\cite{rr}, donc bien avant l'invention
de la differential privacy, afin de mener des enquêtes sur des sujets
sensibles (par exemple, estimer la proportion de tricheurs parmi les
étudiants~\cite{scheers1987improved} ou la proportion de recours à
l'avortement~\cite{abernathy1970estimates}). De nombreux travaux de
recherche actuels élaborent, complexifient, et étudient ce type de
mécanisme afin de pouvoir les utiliser de manière optimale à grande
échelle.


\newpage
\section*{Description du prototype fourni}


\subsubsection*{Cartes personnages}
Un jeu de 18 cartes représentant des dessins de chat. Un chat est
défini par 4 variables pouvant adopter chacune 2 ou 3 modalités :
\begin{itemize}
\item La couleur : beige, violet, bleu.
\item La forme des yeux : fermés, petits, grands.
\item La bouche : petite, grande.
\item Un accessoire : chapeau, ordinateur, monocle.
\end{itemize}

Le jeu de cartes présente différentes distributions en fonction des
variables et des modalités considérés. Par exemple, les chats sont
distribués de manière uniforme sur les couleurs (un tiers des chats
est \enquote{beige}, \enquote{violet}, ou \enquote{bleu}), tandis que
l'accessoire \enquote{chapeau} est biaisé vers les chats bleus.

Une version partielle du jeu de cartes est fournie pour supporter la
variante du jeu dans laquelle l'attaquant a des connaissance
auxiliaires sur les répondants.

\subsection*{Objectifs statistiques}
Quelques objectifs statistiques pouvant être réalisés avec ces
personnages :

\begin{itemize}

\item Combien y a t'il de chats sans ordinateur ? \\
  \textit{Il s'agit d'un dénombrement sur une proportion importante de
    la population.}
  
\item Quelle est la couleur la plus présente chez les chats à chapeau ? \\
  \textit{Il s'agit du calcul d'un max basé sur trois dénombrements
    qui sont déséquilibrés (l'un des dénombrements est
    significativement supérieure aux autres).}
  
\item Les chats beiges ont-ils plus de chance que les autres d'avoir
  les yeux fermés ?  \\
  \textit{Il s'agit ici aussi du calcul d'un max basé sur trois
    dénombrements, mais ces derniers sont cette fois faibles et
    équilibrés (mêmes valeurs).}

\end{itemize}

\subsection*{Objectifs attaques}

Quelques objectifs attaques pouvant être réalisés avec ces
personnages :

\begin{itemize}
  
\item Quels répondants ont un chat bleu ? \\
  \textit{Un ensemble significatif de chats ciblés (un tiers de la
    population).}
  
\item  Quels répondants ont un chat beige avec des yeux fermés ? \\
  \textit{Un ensemble réduit de chats ciblés (un neuvième de la
    population).}

\item  Quel joueur a un chat beige avec un ordinateur ? \\
  \textit{Un unique individu ciblé (un dix-huitième de la
    population).}

\end{itemize}


\bibliography{biblio} \bibliographystyle{plain}



\newpage

\section*{Cartes personnages à découper}

\includegraphics[scale=0.45]{out/37.png}
\includegraphics[scale=0.45]{out/4.png}
\includegraphics[scale=0.45]{out/7.png}
\includegraphics[scale=0.45]{out/10.png}
\includegraphics[scale=0.45]{out/85.png}
\includegraphics[scale=0.45]{out/16.png}
\includegraphics[scale=0.45]{out/56.png}
\includegraphics[scale=0.45]{out/95.png}
\includegraphics[scale=0.45]{out/62.png}
\includegraphics[scale=0.45]{out/29.png}
\includegraphics[scale=0.45]{out/68.png}
\includegraphics[scale=0.45]{out/107.png}
\includegraphics[scale=0.45]{out/75.png}
\includegraphics[scale=0.45]{out/96.png}
\includegraphics[scale=0.45]{out/81.png}
\includegraphics[scale=0.45]{out/84.png}
\includegraphics[scale=0.45]{out/69.png}
\includegraphics[scale=0.45]{out/18.png}

Images : \url{https://github.com/NoahDragon/cat-generator-avatars}

\section*{Connaissances de l'attaquant (ne pas découper)}

\includegraphics[scale=0.45]{out/1.png}
\includegraphics[scale=0.45]{out/4.png}
\includegraphics[scale=0.45]{out/7.png}
\includegraphics[scale=0.45]{out/10.png}
\includegraphics[scale=0.45]{out/13.png}
\includegraphics[scale=0.45]{out/16.png}
\includegraphics[scale=0.45]{out/20.png}
\includegraphics[scale=0.45]{out/23.png}
\includegraphics[scale=0.45]{out/26.png}
\includegraphics[scale=0.45]{out/29.png}
\includegraphics[scale=0.45]{out/32.png}
\includegraphics[scale=0.45]{out/35.png}
\includegraphics[scale=0.45]{out/3.png}
\includegraphics[scale=0.45]{out/24.png}
\includegraphics[scale=0.45]{out/9.png}
\includegraphics[scale=0.45]{out/12.png}
\includegraphics[scale=0.45]{out/33.png}
\includegraphics[scale=0.45]{out/18.png}




Images : \url{https://github.com/NoahDragon/cat-generator-avatars}

\section*{Cartes \enquote{objectif statistique} à découper}

\begin{center}
  \begin{tabular}{|>{\centering\arraybackslash}p{7cm}|>{\centering\arraybackslash}p{7cm}|}
    \hline \vspace{2mm} \Large{Combien y a t'il de chats sans ordinateur ?} \vspace{2mm}
    %      (counts sur une fraction importante de la pop (13/18) => devrait être ok)
    &
      \vspace{2mm} \Large{Quelle est la couleur la plus présente chez les chats à chapeau ?} \vspace{2mm} \\
    %      (max basé sur des counts avec différences de valeurs absolues de counts => devrait être ok)
    \hline \vspace{2mm}
    \Large{Les chats beiges ont-ils plus de chance que les autres d'avoir
    les yeux fermés ?} \vspace{2mm}
%          (max basé sur des counts faibles sans différence de valeur absolue de counts => devrait être difficile)
    & \vspace{2mm} \Large{} \vspace{2mm} \\
    \hline
  \end{tabular}

\end{center}


\section*{Cartes \enquote{objectif attaque} à découper}


\begin{center}
  
  \begin{tabular}{|>{\centering\arraybackslash}p{7cm}|>{\centering\arraybackslash}p{7cm}|}
    \hline \vspace{2mm}
    \Large{Quels joueurs ont un chat bleu ?} \vspace{2mm}&
                                                           %       (déterminer ensemble approximatif, on voit l'intérêt de poser plusieurs requêtes)
                                                           \vspace{2mm} \Large{Quel joueur a le chat beige avec ordinateur ?} \vspace{2mm} \\
    %      (pas évident, besoin de répéter un grand nombre de fois la même question)
    \hline \vspace{2mm}
    \Large{Choisissez un joueur. Quelles sont les caractéristiques du chat de ce joueur ?} \vspace{2mm}& \vspace{2mm} \Large{Quels joueurs ont un chat beige avec des yeux fermés ?} \vspace{2mm} \\
    \hline
  \end{tabular}

\end{center}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
